import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <button class="btn btn-dark" on-click="logout">Logout</button>

      <iron-ajax
        id="doLogout"
        url="http://localhost:3000/apitechu/v2/logout/{{id}}"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="manageAJAXResponse"
        on-error="showError"
      ></iron-ajax>
    `;
  } // End template

  static get properties() {
    return {
      id: {
        type: Number
      }
    };
  } // End properties

  logout(){
    console.log("El usuario ha pulsado el botón");

    console.log("Voy a enviar la petición con los datos del formulario");
    var logoutData = {}

    this.$.doLogout.body = JSON.stringify(logoutData);
    this.$.doLogout.generateRequest();

    console.log("Petición enviada");
  } // End logout

  manageAJAXResponse(data) {
    console.log("manageAJAXResponse");
    console.log(data.detail.response);

    this.dispatchEvent(
      new CustomEvent(
        "evtlogoutsusceed",
        {
          "detail": {
            "userId" : data.detail.response.id,
            "isLogged" : false
          }
        }
      )
    )
  } // End manageAJAXResponse

  showError(error) {
    console.log("Hay un error: showError");
    console.log(error);
    this.id = "";
  } // End showError

} // End class

window.customElements.define('logout-usuario', LogoutUsuario);
