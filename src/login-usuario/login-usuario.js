import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
        /*.test{
          border: solid blue;
        }*/
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Login Usuario</h2>

      <input type="email" placeholder="email" value={{email::input}}></input>
      <input type="password" placeholder="password" value={{password::input}}></input>

      <button class="btn btn-success" on-click="login">Login</button>
      <span hidden$="{{!isLogged}}">Bienvenido/a de nuevo
        <h3>[[first_name]] [[last_name]]</h3>
      </span>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login/"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="manageAJAXResponse"
        on-error="showError"
      ></iron-ajax>

      <!--
      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{id}}"
        handle-as="json"
        on-response="showDataUser"
        on-error="showError"
      ></iron-ajax>
      -->

    `;
  }

  static get properties() {
    return {
      id: {
        type: Number
      },
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String
      },
      password: {
        type: String
      },
      isLogged: {
        type: Boolean,
        value: false
      }
    };
  }

  login(){
    console.log("El usuario ha pulsado el botón");

    console.log("Voy a enviar la petición con los datos del formulario");
    var loginData = {
      "email" : this.email,
      "password" : this.password
    }
    console.log(loginData);

    this.isLogged = false;

    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();

    console.log("Petición enviada");
  }

  manageAJAXResponse(data) {
    console.log("manageAJAXResponse");
    console.log(data.detail.response);

    this.isLogged = true;

    this.dispatchEvent(
      new CustomEvent(
        "evtloginsusceed",
        {
          "detail": {
            "userId" : data.detail.response.id,
            "isLogged" : true
          }
        }
      )
    )

    // this.$.getUser.generateRequest();
    // this.first_name = data.detail.response.first_name;
    // this.last_name = data.detail.response.last_name;
    // this.email = data.detail.response.email;
  }

  // showDataUser(data) {
  //   console.log("showDataUser");
  //   console.log(data.detail.response);
  //
  //   this.first_name = data.detail.response.first_name;
  //   this.last_name = data.detail.response.last_name;
  // }

  showError(error) {
    console.log("Hay un error: showError");
    console.log(error);
    this.id = "";
    this.first_name = "";
    this.last_name = "";

  }

  sendEvent (event) {
    console.log("El usuario ha pulsado el botón");
    console.log(event);

    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail": {
            "course" : "TechU",
            "year" : 2019,
            "edition" : 11
          }
        }
      )
    )
  }

}

window.customElements.define('login-usuario', LoginUsuario);
