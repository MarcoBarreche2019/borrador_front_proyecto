import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h3>Hola, soy [[first_name]] [[last_name]], y mi email es [[email]]</h3>

      <span hidden$="{{viewAccounts}}">
        <button class="btn btn-dark btn-xs" on-click="verCuentas">Ver cuentas</button></br>
      </span>

      <span hidden$="{{!viewAccounts}}">
        <button class="btn btn-dark btn-xs" on-click="ocultarCuentas">Ocultar cuentas</button></br>
        <visor-cuentas id="visorcuentas"></visor-cuentas>
      </span>

      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{id}}"
        handle-as="json"
        on-response="showData"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      id: {
        type: Number,
        observer: "_idChanged"
      },
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String
      },
      viewAccounts: {
        type: Boolean,
        value: false
      }
    };
  } // End properties

  _idChanged(newValue, oldValue) {
    console.log("Course value has changed");
    console.log("Old value was " + oldValue);
    console.log("New value is " + newValue);
    if (!isNaN(newValue)) {
      this.$.getUser.generateRequest();
    }
  } // End _idChanged (observer)

  showData(data) {
    console.log("showData");
    console.log(data.detail.response);
    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;
  } // End showData

  verCuentas () {
    this.$.visorcuentas.id = this.id;
    this.viewAccounts = true;
  }

  ocultarCuentas () {
    this.$.visorcuentas.accounts = [];
    this.$.visorcuentas.id = "";
    this.viewAccounts = false;
  }

} // End class

window.customElements.define('visor-usuario', VisorUsuario);
