import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
        /*.test{
          border: solid blue;
        }*/
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Soy el emisor</h2>
      <button class="btn btn-success" on-click="sendEvent">NO Pulsar</button>
    `;
  }

  static get properties() {
    return {
    };
  } // End properties

  sendEvent (event) {
    console.log("El usuario ha pulsado el botón");
    console.log(event);

    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail": {
            "course" : "TechU",
            "year" : 2019,
            "edition" : 11
          }
        }
      )
    )
  }
} // End class

window.customElements.define('emisor-evento', EmisorEvento);
