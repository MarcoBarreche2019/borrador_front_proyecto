import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

import '@vaadin/vaadin-button';
import '@vaadin/vaadin-grid';
import '@vaadin/vaadin-text-field';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

          <div> Listado de cuentas: </div>
          <vaadin-grid id="cuentas" height-by-rows theme="row-stripes">
            <vaadin-grid-column path="IBAN" header="IBAN"></vaadin-grid-column>
            <vaadin-grid-column path="alias" header="Alias"></vaadin-grid-column>
            <vaadin-grid-column path="balance" header="Balance"></vaadin-grid-column>
          </vaadin-grid>

      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/v2/accounts/{{id}}"
        handle-as="json"
        on-response="showData"
      ></iron-ajax>
    `;
  }

  static get is() { return 'account-list'; }

  static get properties() {
    return {
      id: {
        type: Number,
        observer: "_idChanged"
      },
      accounts: {
        type: Array
      }
    };
  }

  _idChanged(newValue, oldValue) {
    console.log("Course value has changed");
    console.log("Old value was " + oldValue);
    console.log("New value is " + newValue);
    if (!isNaN(newValue) && newValue!="") {
      this.$.getAccounts.generateRequest();
    }
  } // End _idChanged (observer)

  showData(data) {
    console.log("showData");
    console.log(data.detail.response);
    this.accounts = data.detail.response;
    this.$.cuentas.items = this.accounts;
  } // End showData

}

window.customElements.define('visor-cuentas', VisorCuentas);
